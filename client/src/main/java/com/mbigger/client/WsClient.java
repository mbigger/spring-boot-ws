package com.mbigger.client;

import com.unismc.client.GetCountryRequest;
import com.unismc.client.GetCountryResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class WsClient extends WebServiceGatewaySupport {
    public GetCountryResponse getCountry(String name) {
        GetCountryRequest request = new GetCountryRequest();
        request.setName(name);
        GetCountryResponse response = (GetCountryResponse) getWebServiceTemplate().marshalSendAndReceive(
                "http://localhost:9000/ws/countries.wsdl", request);
        return response;
    }
}
